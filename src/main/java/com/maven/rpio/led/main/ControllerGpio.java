/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maven.rpio.led.main;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
/**
 *
 * @author renzo
 */
public class ControllerGpio {
    public void ControllerGpioExample() throws InterruptedException {
        
        final GpioController gpio = GpioFactory.getInstance();
        
        final GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "My Led", PinState.HIGH);
        
        pin.setShutdownOptions(true, PinState.LOW);
        
        Thread.sleep(5000);
        
        pin.low();
        
        Thread.sleep(5000);
        
        pin.toggle();
        
        Thread.sleep(5000);
        
        pin.pulse(1000, true);
        
        gpio.shutdown();
    }
}
